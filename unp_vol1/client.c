#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char const* argv[])
{
    int socketfd, n;
    struct sockaddr_in servaddr;
    char buf[256];

    //创建套接字
    socketfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socketfd < 0) {
        printf("[CLIENT] create socket failed.\n");
        return -1;
    }

    // 设置服务器地址
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("192.168.1.100");
    servaddr.sin_port = htons(8000);

    // 链接
    if(connect(socketfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0){
        printf("[CLIENT] connect failed.\n");
        return -1;
    }
    strcpy(buf, "msg from client.");
    if(send(socketfd, buf, sizeof(buf),0) < 0){
        printf("[CLIENT] send failed.\n");
        close(socketfd);
        return -1;
    }
    n = recv(socketfd, buf, sizeof(buf), 0);
    if(n > 0){
        buf[n] = '\0';
        printf("[CLIENT] recv: %s\n", buf);
    }

    close(socketfd);

    return 0;
}
