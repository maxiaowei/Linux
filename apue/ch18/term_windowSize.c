/**
 * 捕捉窗口大小改变的信号，并打印
 **/
#include "apue.h"

static void pr_winsize(int fd)
{
  struct winsize sz;
  ioctl(fd, TIOCGWINSZ, &sz);
  printf("rows %d; columns %d\n", sz.ws_row, sz.ws_col);
}

static void win_change(int signo)
{
  printf("size changed: ");
  pr_winsize(STDIN_FILENO);
}

int main()
{
  // 判断是否是终端
  if (!isatty(STDIN_FILENO)) {
    return -1;
  }
  printf("current size: ");
  pr_winsize(STDIN_FILENO);
  // 绑定信号处理函数
  signal(SIGWINCH, win_change);

  while (1)
    pause();

  return 0;
}