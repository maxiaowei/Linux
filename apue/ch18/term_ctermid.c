#include "apue.h"

int main()
{
  char name[L_ctermid] = {0};

  // 获取终端名字
  char *pc = ctermid(name);
  if (pc != NULL) {
    printf("%s\n", pc == name ? "yes" : "no");
    printf("[0] %s\n", pc);
    printf("[1] %s\n", name);
  }

  char *pname = ctermid(NULL);
  if (pname != NULL) {
    printf("[3] %s\n", pname);
  }

  printf("%p %p %p\n", name, pc, pname);

  return 0;
}