/**
 * 非规范模式下，MIN和TIME值的使用
 **/
#include "apue.h"

#include <termios.h>

#define MIN  (5)
#define TIME (10)

int main()
{
  char buf[64];
  struct termios set;

  // 设置参数
  tcgetattr(STDIN_FILENO, &set);
  set.c_lflag &= ~ICANON;  // 非规范模式
  set.c_cc[VMIN] = MIN;
  set.c_cc[VTIME] = TIME;
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &set);

  // 验证
  tcgetattr(STDIN_FILENO, &set);
  if (set.c_cc[VMIN] != MIN || set.c_cc[VTIME] != TIME) {
    printf("setattr failed.\n");
  }
  sleep(3);   /* 模拟已有数据的情况下，读取的字符数 */
  // 读取
  ssize_t n = read(STDIN_FILENO, buf, 32);
  if (n > 0) {
    buf[n] = 0;
    printf("\nread %d bytes:%s\n", (int)n, buf);
  }

  return 0;
}