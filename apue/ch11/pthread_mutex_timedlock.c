#include <pthread.h>
#include <time.h>

#include "apue.h"

int main()
{
  int err;
  struct timespec tout;
  struct tm *tmp;
  char buf[64];
  pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

  // 加锁
  pthread_mutex_lock(&lock);
  printf("mutex is locked.\n");
  clock_gettime(CLOCK_REALTIME, &tout);
  tmp = localtime(&tout.tv_sec);
  strftime(buf, sizeof(buf), "%r", tmp);
  printf("current time is %s\n", buf);

  // 设置超时
  tout.tv_sec += 3;
  err = pthread_mutex_timedlock(&lock, &tout);
  clock_gettime(CLOCK_REALTIME, &tout);
  tmp = localtime(&tout.tv_sec);
  strftime(buf, sizeof(buf), "%r", tmp);
  printf("the time is now %s\n", buf);

  if (err == 0) {
    printf("mutex locked.\n");
  } else {
    printf("can't lock mutex:%s\n", strerror(err));
  }

  return 0;
}