#!/bin/sh
# The script(1) program normally adds to the beginning of the output file a line with the
# starting time, and to the end of the output file another line with the ending time. Add
# these features to the simple shell script that we showed.
(echo "Script started on " `date`;
../build/bin/pty "${SHELL:-/bin/sh}";
echo "Script done on " `date`) | tee typescript