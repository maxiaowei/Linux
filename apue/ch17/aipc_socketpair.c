#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main()
{
  int fd[2];
  int n, i;
  char *str1 = "12345";
  char *str2 = "56789";
  char buf[10];

  if (socketpair(AF_UNIX, SOCK_DGRAM, 0, fd) == -1) {
    printf("socketpair failed.\n");
  }

  // 一读一写
  n = write(fd[0], str1, strlen(str1));
  if (n > 0) {
    read(fd[1], buf, n);
    buf[n] = 0;
    printf("[1] %s\n", buf);
  }

  // 同时读写
  n = write(fd[0], str1, strlen(str1));
  i = write(fd[1], str2, strlen(str2));
  if (n > 0) {
    read(fd[1], buf, n);
    buf[n] = 0;
    printf("[2] %s\n", buf);
  }
  if(i > 0){
      read(fd[0], buf, i);\
      buf[i] = 0;
      printf("[3] %s\n", buf);
  }

  return 0;
}