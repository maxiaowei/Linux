/**
 * Determine the capacity of a pipe using nonblocking writes. Compare this value with the
 * value of PIPE_BUF from Chapter 2.
 **/

#include "apue.h"

#include <fcntl.h>
#include <limits.h>

int main()
{
  int fd[2];
  int n = 0;

  if (pipe(fd) < 0) {
    err_sys("pipe error");
  }
  if (fcntl(fd[1], F_SETFL, O_NONBLOCK) < 0) {
    err_sys("fcntl error");
  }

  while (n < INT_MAX) {
    if (write(fd[1], "a", 1) != 1) {
      break;
    }
    n++;
  }

  printf("capacity of a pipe is %d.\n", n);
  return 0;
}