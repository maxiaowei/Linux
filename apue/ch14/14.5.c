/**
 * Implement the function sleep_us, which is similar to sleep, but waits for a specified
 * number of microseconds. Use either select or poll. Compare this function to the BSD
 * usleep function.
 **/

#include "apue.h"

#include <sys/time.h>

#define US_OF_SECOND (1000000)

void sleep_us(long int us)
{
  struct timeval t;

  t.tv_sec = us / US_OF_SECOND;
  t.tv_usec = us % US_OF_SECOND;

  select(0, NULL, NULL, NULL, &t);
}

int main()
{
  // 可以使用time命令查看运行时间
  sleep_us(100000);
  return 0;
}