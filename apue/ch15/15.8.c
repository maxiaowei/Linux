/**
 * What happens if the cmdstring executed by popen with a type of "r"
 * writes to its standard error?
 * 不加重定向的话，子程序的输出不会通过管道传给调用进程。
 **/
#include "apue.h"

#define BUFSIZE (2048)

int main()
{
  FILE *fp;
  char buf[BUFSIZE] = {0};
  const char *str = "main:";
  int n = strlen(str);

  fp = popen("./15.8_cmd 2>&1", "r");
  if (fp == NULL) {
    printf("popen failed.\n");
  }

  memcpy(buf, str, n);
  if (fgets(buf + n, BUFSIZ - n, fp) != NULL) {
    n = strlen(buf);
    buf[n] = 0;
    fprintf(stderr, buf);
    fflush(stderr);
  }
  return 0;
}